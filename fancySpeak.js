mw.addBinding("rainbow",1,1,",rainbow [string] - paints mw with all the colours of the rainbow",function(str){
    let colours = ["R-","Y-","G-","C-","B-","M-"],
        format_code = "\033",
        step = str.length/colours.length,
        rtn = "";
    for(let i=0;i<colours.length;i++){
        let frm = Math.round(i*step),
            too = Math.round((i+1)*step);
        rtn += format_code+colours[i] + str.substring(frm,too);
    }
    say(rtn);
});
