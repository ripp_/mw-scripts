/*globals
    mw:true,
    bind:false,
    print:false
*/
/* exported
    mw,
    handleEvent,
    handleBind
*/
const mw = (function(){
    //Set a local mw, this means that through the code we can always use mw
    let mw={};

    //Persistance system, currently backed with Store - which sometimes causes timeouts
    let store = {};
    mw.assert = function(test){if(!test){print("\033R- FATAL ERROR during setup of mw namespace");} return !test;};
    mw.persistance = function(namespace,props){
        if (mw.assert(namespace.indexOf(".") === -1,"Namepsace %s contains forbiddeon character '.'",namespace) ||
            mw.assert(store[namespace] === undefined,"Namespace %s already in use",namespace)) {
                return;
        }

        let namespaceStore = {},
            keys = Object.keys(props);
        store[namespace] = namespaceStore;

        for(let i=0;i<keys.length;i++){
            let key = keys[i],
                value = props[key];
                    if (mw.assert(key.indexOf(".") === -1,"Property %s in %s contains forbiddeon character '.' - skipping it",key,namespace)) {
                        return;
                    }
                    Object.defineProperty(namespaceStore,key,{
                        get: function(){
                            return JSON.parse(Store[namespace+"."+key]);
                        }, // jshint ignore:line
                        set: function(v){
                            Store[namespace+"."+key] = JSON.stringify(v);
                        }, // jshint ignore:line
                        enumerable:true
                    });
                    if (Store[namespace+"."+key] === undefined){
                        namespaceStore[key] = value;
                    }
        }
        return Object.seal(namespaceStore);
    };

    //Define the persistance variables
    let mwStore = mw.persistance("mw",{
        "infoEnabled":false,
        "warnEnabled":true,
        "errorEnabled":true,
        "gapOutput":true
    });

    let onDebugs = false;

    function formatObjectsArray(objects){
        let formatted = "";
        for (let i=0;i<objects.length;i++){
            if(typeof objects[i] === "string"){
                formatted += (formatted?" ":"") + objects[i].replace(/%(.)/g,function (full,type){
                    if (type === "%"){
                        return "%";
                    } else if (type === "s"){
                        return objects[++i];
                    } else if (type === "c"){
                        return "\033";
                    } else {
                        return full;
                    }
                }); // jshint ignore:line
            } else {
                formatted += (formatted?" ":"") + objects[i];
            }
        }
        return formatted;
    }

    function startOutType(isDebug){
        if(onDebugs !== isDebug){
            if (mwStore.gapOutput){
                print(" ");
            }
            onDebugs = isDebug;
        }
    }

    mw.assert = function(test){
        if (!test){
            startOutType(true);
            print("\033R-" + formatObjectsArray(Array.prototype.slice.call(arguments,1)));
        }
        return !test;
    };
    mw.error = function(){
        if (mwStore.errorEnabled){
            startOutType(true);
            print("\033R-" + formatObjectsArray(arguments));
        }
    };
    mw.info = function(){
        if (mwStore.infoEnabled){
            startOutType(true);
            print("\033B-" + formatObjectsArray(arguments));
        }
    };
    mw.log = function(){
        startOutType(false);
        print(formatObjectsArray(arguments));
    };
    mw.warn = function(){
        if (mwStore.warnEnabled){
            startOutType(true);
            print("\033Y-" + formatObjectsArray(arguments));
        }
    };

    //Event System
    let events = {};
    mw.addEvent = function(kind,func){
        if (events[kind] === undefined){
            events[kind] = [];
        }
        events[kind].push(func);
    };

    mw._handleEvent = function (type,user,text){
        let selectedEvents = events[type];
        if (selectedEvents === undefined) {
            return true;
        }
        for(let i=0;i<selectedEvents.length;i++){
            let nText = selectedEvents[i](type,user,text);
            if (nText !== undefined){
                text = nText;
            }
        }
        mw.log(text);
        return false;
    };

    //Bindings
    let binds = {};
    mw.addBinding = function(name,minArgs,maxArgs,helpMesg,func){
        if (mw.assert(minArgs >= 0,"minArgs cannot be negative (binding %s)",name) ||
            mw.assert(maxArgs >= minArgs,"maxArgs cannot be less than minArgs (binding %s)",name) ||
            mw.assert(!binds[name],"%s is alread bound to a function",name)){
                return;
        }
        binds[name] = [func,minArgs,maxArgs,helpMesg];
        bind(name,"handleBind");
    };

    mw._handleBind = function(name){
        let info = binds[name];
        if (info !== undefined){
            let func = info[0],
                minArgs = info[1],
                maxArgs = info[2],
                helpString = info[3];
            if (arguments.length - 1 < minArgs){
                mw.log("Bind %s takes atleast %s arguments but only %s given",name,minArgs,arguments.length-1);
                mw.log(helpString);
            } else {
                let args = [],
                    lastArgs = [];
                for(let i=1;i<arguments.length;i++){
                    if (i<maxArgs){
                        args.push(arguments[i]);
                    } else {
                        lastArgs.push(arguments[i]);
                    }
                }
                if (lastArgs){
                    args.push(lastArgs.join(" "));
                }
                if (!func.apply(this,args)){
                    mw.log(helpString);
                }
            }
        } else {
            mw.log("No function bound to %s",name);
            return;
        }
    };

    //mw's own bindings
    mw.addBinding("help",0,0,",help - diplays help strings",function(){
        let cmds = Object.keys(binds);
        for(let i=0;i<cmds.length;i++){
            mw.log(binds[cmds[i]][3]);
        }
        return true;
    });

    mw.addBinding("debug",0,1,",debug [on|off] OR [+|-][iwe] - turn debug info on or off",function(on){
        if (on === "on"){
            mwStore.infoEnabled = true;
            mwStore.warnEnabled = true;
            mwStore.errorEnabled = true;
        } else if (on === "off") {
            mwStore.infoEnabled = false;
            mwStore.warnEnabled = false;
            mwStore.errorEnabled = false;
        } else if (on) {
            let mode = true;
            for(let i=0;i<on.length;i++){
                switch(on.substr(i,1)){
                    case "+": mode=true; break;
                    case "-": mode=false; break;
                    case "i": mwStore.infoEnabled = mode; break;
                    case "w": mwStore.warnEnabled = mode; break;
                    case "e": mwStore.errorEnabled = mode; break;
                    default: return false;
                }
            }
        }
        mw.log("Debug logs set to: %s%s%s",mwStore.infoEnabled?"\033B-i":"\033---",mwStore.warnEnabled?"\033Y-w":"\033---",mwStore.errorEnabled?"\033R-e":"\033---");
        return true;
    });

    //TODO expand this so we have 3 blocks - chat, log from commands and debugs
    mw.addBinding("gap",0,1,",gap [on|off] - toggles adding a gap between blocks of console.log and the debug ouputs",function(on){
        if (on === "on"){
            mwStore.gapOutput = true;
        } else if (on === "off"){
            mwStore.gapOutput = false;
        } else if (on){
            return false;
        }
        mw.log("Debug output sepearation is turned",mwStore.gapOutput?"on":"off");
        return true;
    });

    mw.addBinding("_debugTest",0,0,",_debugTest - print all the types of mw.js outputs",function(){
        mw.log("log");
        mw.info("info");
        mw.warn("warning");
        mw.error("error");
        mw.assert(0,"failed assertsion");
        return true;
    });

    mw.addBinding("_inspect",0,1,",_inspect - print values stored in object",function(key){
        if (key){
            let space = store[key],
                keys = Object.keys(space);
            mw.log("Namespace %s:",key);
            for(let i=0;i<keys.length;i++){
                mw.log("  %s: %s",keys[i],space[keys[i]]);
            }
        } else {
            let keys = Object.keys(store);
            mw.log("List of namespaces:");
            for(let i=0;i<keys.length;i++){
                mw.log("  %s",keys[i]);
            }
        }
        return true;
    });
    return Object.freeze(mw);
})();

//This is because we can't refrence mw.thing in .mwrc
const handleEvent = mw._handleEvent;
//And likewise for the bind function
const handleBind = mw._handleBind;
