(function(){
    let lastMessagesDay = 0;

    function timestampEvent (type,user,text){
        let n = new Date(),
            h = n.getHours(),
            m = n.getMinutes(),
            s = n.getSeconds();
        h = h<10?"0"+h:h;
        m = m<10?"0"+m:m;
        s = s<10?"0"+s:s;
        let D = Math.floor(n/(1000*60*60*24));
        if (D !== lastMessagesDay){
            lastMessagesDay = D;
            print("\033W- # "+n.toDateString()+" # \033--");
        }

        return h+":"+m+":"+s+" "+text;
    }

    mw.addEvent("text",timestampEvent);
})();
