(function(){
    const PAD_SIZE = 17;

    function pad(string,length,padLeft){
        let padAmount = length - string.length;
        if (padAmount <= 0){
            return string;
        } else {
            let padding = new Array(padAmount+1).join(" ");
            if (padLeft){
                return string + padding;
            } else {
                return padding + string;
            }
        }
    }

    function rawDetect (type,user,text){
        let nameOffset = 0,
            combine = "?";
        if (text.substr(0,1) === "\033"){
            nameOffset = 3;
        }
        if (text.substr(nameOffset,user.length) !== user){
            user = user + ">";
        } else {
            text = text.substr(nameOffset+user.length);
            // ' |'s |'d |'ll |:
            if ( /^('(s|d|ll)?|:) /.test(text)){
                user = user+RegExp.lastMatch.substr(0,1);
                text = RegExp.$2+" "+RegExp.rightContext;
            } else if (/^ (says|whispers|asks): /.test(text)){
                //Private chat
                user += "~";
                text = " "+RegExp.rightContext;
            } else if (/^ shouts: /.test(text)){
                user += "@";
                text = " "+RegExp.rightContext;
            } else if (text.substr(0,1) === " ") {
                user += " ";
                text = " "+text.substr(1);
            } else {
                combine = "?";
            }
        }
        return pad(user,PAD_SIZE) + text;
    }

    mw.addEvent("text",rawDetect);
})();
