(function(){
    let FORMAT_CODE = "\033",
        RESET = FORMAT_CODE+"--",
        COLOURS = ["W-","G-","B-","M-","g-","Y-","C-","c-","m-","r-","K-"],
        COLS_PER_NAME = 2,
        MIN_CHARS_PER_COL = 3,
        userMap = {};

    function userCheckEvent (type,user,text){ // jshint ignore:line
        if (!(user in userMap)){
            storeColoursForUser(user);
        }
    }

    function colourEvent (type,user,text) {
        return text.replace(/[a-zA-Z0-9_]+/g,colourizeName);
    }

    function colourizeName(w){
        let cols = userMap[w];
        if (cols === undefined){
            return w;
        } else {
            let rtn = "",
                step = w.length/cols.length;
            for(let i=0;i<cols.length;i++){
                rtn += FORMAT_CODE+COLOURS[cols[i]]+w.substring(Math.round(step*i),Math.round(step*(i+1)));
            }
            return rtn+RESET;
        }
    }

    function storeColoursForUser(user){
        let hash = Math.abs(user.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a;},0)),
            cols = Math.floor(user.length/MIN_CHARS_PER_COL);
        if (cols>COLS_PER_NAME){
            cols = COLS_PER_NAME;
        } else if (cols <= 0){
            cols = 1;
        }
        userMap[user] = Array(cols);
        for(let i=0;i<cols;i++){
            userMap[user][i]=hash%(COLOURS.length);
            hash = Math.floor(hash/COLOURS.length);
        }
    }

    function getUsers() {
        for (let wl = wholist(), i = 0; i < wl.length; i++) {
            storeColoursForUser(wl[i].username);
        }
    }

    mw.addEvent("text",userCheckEvent);
    mw.addEvent("text",colourEvent);
    getUsers();
})();
