# About
This repo contains mw.js - a wrapper and helper system for the millyways javscript system along with several example and helpful millyways scripts that use it.

# Usage

## Linking to millyways
 1. Save the repo to your home directory, ideally to `~/mw-script` (which may contain old mw scripts, which can be deleted or backup)
 2. Replace your `.mwrc` file (by default located in your home directory) to the one included in the repo.

## Adding owns scripts
Create a new js file containing the code you need.
Your code should be wrapped in an anonymous function to protect the global name space.
Add an `include` line to your `.mwrc` with the location of the new script, insure it is added after `include mw-script/mw.js` or the `mw` object will not be available to it.
