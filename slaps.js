(function(){
    function pick(list){
        return list[Math.floor(Math.random()*list.length)];
    }

    function getSlap(user){
        //simple coumpound slaps
        let adjectives = [
            "hits with",
            "with",
            "around a bit with",
            "hard with"
        ], objects = [
            "a large trout",
            "a minnow",
            "a whale",
            "a small trout",
            "a herring",
            "a shrubbery",
            "a side of chips",
            "silver",
            "iridium",
            "Imranh's cap",
            "the old rack",
            "the guide",
            "Door",
            "Printer",
            "the old crap chairs",
            "guestnet",
            "all the outdated books in the SUCS libary",
            "a macbook",
            "LART",
            "a Vogon constructor fleet",
            "a Vogon's poetry",
            "Zaphod Beeblebrox's third hand",
            "Eddie, your friendly shipboard computer",
            "Zaphod Beeblebrox's ego",
            "a glitter bomb, and now there is sparkly shit in their eyes."
        ];
        let adj = pick(adjectives),
            obj = pick(objects);
        return "Slaps "+user+" "+adj+" "+obj;
    }

    mw.addBinding("slap",1,1,",slap [user] - bring the pain",function(user){
        if (user === undefined){
            return false;
        }
        exec("e "+getSlap(user));
        return true;
    });
})();
